package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// branch.lstファイル表示名
	private static final String FILE_NAME_BRANCH = "支店";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// commodity.lstファイル表示名
	private static final String FILE_NAME_COMMODITY = "商品";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// 支店定義ファイルの仕様
	private static final String BRANCH_REGULAR_EXPRESSIONS = "^\\d{3}";

	// 商品定義ファイルの仕様
	private static final String COMMODITY_REGULAR_EXPRESSIONS = "^([A-Za-z0-9]){8}$";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String FILE_INVALID_CODE = "の支店コードが不正です";
	private static final String FILE_INVALID_code = "の商品コードが不正です";
	private static final String FILE_INVALID_format = "のフォーマットが不正です";
	private static final String FILE_NOT_SERIAL_NUMBER = "売上ファイル名が連番になっていません";
	private static final String FILE_OVER_TOTAL = "合計金額が10桁を超えました";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		//コマンドライン引数が渡されているか確認
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();



		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales,BRANCH_REGULAR_EXPRESSIONS,FILE_NAME_BRANCH)) {
			return;
		}

		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales,COMMODITY_REGULAR_EXPRESSIONS,FILE_NAME_COMMODITY)) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)

		//売り上げファイルの読み込み
		//ディレクトリにある全てのファイルを取得
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<File>();

		//売上ファイルの条件に当てはまるものを判定
		for(int i = 0; i < files.length ; i++ ) {

			String fileName = files[i].getName();

			//ファイルから選んだものと8桁の数字＋拡張子を一致させる
			//ファイルとディレクトリのどちらなのか確認
			if(files[i].isFile() && fileName.matches("^\\d{8}$*.rcd$")) {

				//判定して一致したファイルを保持
				rcdFiles.add(files[i]);
			}
		}

		//売上ファイルが連番か確認
		Collections.sort(rcdFiles);
		for(int i = 0; i <rcdFiles.size() - 1; i++) {

			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i+1).getName().substring(0,8));

			if((latter - former) != 1) {
				System.out.println(FILE_NOT_SERIAL_NUMBER);
				return;
			}
		}

		//ファイルの読込が必要。rcdFilesから繰り返しファイルを取り出す
		for(int i = 0; i < rcdFiles.size(); i++) {
			BufferedReader br = null;
			try {
				File rcdFile = rcdFiles.get(i);
				FileReader fr = new FileReader(rcdFile);
				br = new BufferedReader(fr);

				String line;
				//Listを用意する
				List<String> rcdfile = new ArrayList<String>();
				while((line = br.readLine()) != null) {
					rcdfile.add(line);
				}
				//売上ファイルのフォーマットを確認
				if(rcdfile.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + FILE_INVALID_format);
					return;
				}

				//売上ファイルの支店コードが支店定義ファイルに存在するか確認
				if(!branchSales.containsKey(rcdfile.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + FILE_INVALID_CODE);
					return;
				}

				//売上ファイルの商品コードが商品定義ファイルに存在するか確認
				if(!commoditySales.containsKey(rcdfile.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + FILE_INVALID_code);
					return;
				}

				//売上金額が数字なのか確認
				if(!rcdfile.get(2).matches("^[0-9]*$")) {
					System.out.println(rcdFiles.get(i).getName() + UNKNOWN_ERROR);
					return;
				}

				//読み込んだファイルをLongに変換し、支店コードと売上金を識別し、計上
				long fileSale = Long.parseLong(rcdfile.get(2));
				Long branchAmount = branchSales.get(rcdfile.get(0)) + fileSale;

				//商品コードと売上金を識別し、計上
				Long commodityAmount = commoditySales.get(rcdfile.get(1)) + fileSale;

				//売上金額が10桁を超えたか確認
				if((branchAmount >= 10000000000L) || (commodityAmount >= 10000000000L)) {
					System.out.println(FILE_OVER_TOTAL);
					return;
				}

				//Mapに追加する処理が必要。key:コード、value:売上金額
				branchSales.put(rcdfile.get(0),branchAmount);
				commoditySales.put(rcdfile.get(1),commodityAmount);

			}catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> Names, Map<String, Long> Sales,String specification,String display) {
		BufferedReader br = null;

		//定義ファイルが存在しない場合、エラー表示
		File fileList = new File(path, fileName);
		if(!fileList.exists()) {
			System.out.println(display + FILE_NOT_EXIST);
			return false;
		}

		try {
			File file = new File(path, fileName);
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");

				//配列の中身が仕様と一致するかチェックする
				if((items.length != 2) || (!items[0].matches(specification))){
					System.out.println(display + FILE_INVALID_FORMAT);
					return false;
				}

				Names.put(items[0], items[1]);
				Sales.put(items[0], 0L);

			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> Names, Map<String, Long> Sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;
		//MapからKeyを取得する
		try {
			File file = new File(path,fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for(String key : Sales.keySet()) {
				//*出力は「コード」,「名前」,「売上金額の合計」
				bw.write(key + "," + Names.get(key) + "," + Sales.get(key));
				bw.newLine();
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
